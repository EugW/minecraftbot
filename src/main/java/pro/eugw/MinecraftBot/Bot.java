package pro.eugw.MinecraftBot;

import com.github.steveice10.mc.protocol.MinecraftProtocol;
import com.github.steveice10.mc.protocol.data.message.Message;
import com.github.steveice10.mc.protocol.packet.ingame.client.ClientChatPacket;
import com.github.steveice10.mc.protocol.packet.ingame.server.ServerChatPacket;
import com.github.steveice10.mc.protocol.packet.ingame.server.world.ServerMapDataPacket;
import com.github.steveice10.packetlib.Client;
import com.github.steveice10.packetlib.event.session.DisconnectedEvent;
import com.github.steveice10.packetlib.event.session.PacketReceivedEvent;
import com.github.steveice10.packetlib.event.session.SessionAdapter;
import com.github.steveice10.packetlib.tcp.TcpSessionFactory;

import javax.imageio.ImageIO;
import javax.swing.*;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.net.InetSocketAddress;
import java.net.Proxy;

import static pro.eugw.MinecraftBot.Misc.log;

class Bot extends BotThread {

    private String USERNAME;
    private String PASSWORD;
    private String APIKEY;
    private Client CLIENT;
    private JTextArea textArea;

    Bot(String ip, String user, String api_key, String proxy) throws Exception {
        USERNAME = user.split(":")[0];
        PASSWORD = user.split(":")[1];
        APIKEY = api_key;
        MinecraftProtocol protocol = new MinecraftProtocol(USERNAME);
        CLIENT = new Client(ip.split(":")[0], Integer.valueOf(ip.split(":")[1]), protocol, new TcpSessionFactory(new Proxy(Proxy.Type.HTTP, new InetSocketAddress(proxy.split(":")[0], Integer.valueOf(proxy.split(":")[1])))));
        addListeners();
    }


    @Override
    public void command(String cmd1, JTextArea textArea1) {
        if (!cmd1.isEmpty()) {
            textArea = textArea1;
            log().debug("Command sent");
            CLIENT.getSession().send(new ClientChatPacket(cmd1));
        }
    }

    @Override
    public void interrupt() {
        log().debug("Thread stopped");
        CLIENT.getSession().disconnect("stopped");
    }

    @Override
    public void run() {
        log().debug("Thread started");
        CLIENT.getSession().connect();
    }

    private void addListeners() {
        CLIENT.getSession().addListener(new SessionAdapter() {
            @Override
            public void packetReceived(PacketReceivedEvent event) {
                if (event.getPacket() instanceof ServerMapDataPacket) {
                    if (((ServerMapDataPacket) event.getPacket()).getData() != null) {
                        File dir = new File("images");
                        if (!dir.exists()) {
                            if (dir.mkdirs())
                                log().debug("Images dir created");
                        }
                        File file = new File(dir, USERNAME + ".png");
                        BufferedImage bufferedImage = new BufferedImage(((ServerMapDataPacket) event.getPacket()).getData().getColumns(), ((ServerMapDataPacket) event.getPacket()).getData().getRows(), BufferedImage.TYPE_INT_RGB);
                        drawMapImage(bufferedImage, ((ServerMapDataPacket) event.getPacket()).getData().getData());
                        try {
                            ImageIO.write(bufferedImage, "png", file);
                        } catch (Exception e) {
                            log().error(e);
                        }
                        CLIENT.getSession().send(new ClientChatPacket(new SolveCaptcha(String.valueOf(file), APIKEY).get()));
                    }
                }
                if(event.getPacket() instanceof ServerChatPacket) {
                    if (((ServerChatPacket) event.getPacket()).getMessage().getFullText().contains("/reg")) {
                        CLIENT.getSession().send(new ClientChatPacket("/reg " + PASSWORD + " " + PASSWORD));
                    }
                    if (((ServerChatPacket) event.getPacket()).getMessage().getFullText().contains("/l")) {
                        CLIENT.getSession().send(new ClientChatPacket("/l " + PASSWORD + " " + PASSWORD));
                    }
                    log().debug(USERNAME + ": " + ((ServerChatPacket) event.getPacket()).getMessage().getFullText());
                    if (textArea != null)
                        textArea.append(USERNAME + ": " + ((ServerChatPacket) event.getPacket()).getMessage().getFullText() + "\n");
                }
            }
            @Override
            public void disconnected(DisconnectedEvent event) {
                log().info(USERNAME + ": Disconnected: " + Message.fromString(event.getReason()).getFullText());
                if(event.getCause() != null) {
                    log().error(event.getCause());
                    event.getCause().printStackTrace();
                }
            }
        });
    }

    private void drawMapImage(BufferedImage image, byte[] img) {
        Color[] colors = new Color[]{new Color(0, 0, 0), new Color(0, 0, 0), new Color(0, 0, 0), new Color(0, 0, 0), new Color(89, 125, 39), new Color(109, 153, 48), new Color(127, 178, 56), new Color(67, 94, 29), new Color(174, 164, 115), new Color(213, 201, 140), new Color(247, 233, 163), new Color(130, 123, 86), new Color(140, 140, 140), new Color(171, 171, 171), new Color(199, 199, 199), new Color(105, 105, 105), new Color(180, 0, 0), new Color(220, 0, 0), new Color(255, 0, 0), new Color(135, 0, 0), new Color(112, 112, 180), new Color(138, 138, 220), new Color(160, 160, 255), new Color(84, 84, 135), new Color(117, 117, 117), new Color(144, 144, 144), new Color(167, 167, 167), new Color(88, 88, 88), new Color(0, 87, 0), new Color(0, 106, 0), new Color(0, 124, 0), new Color(0, 65, 0), new Color(180, 180, 180), new Color(220, 220, 220), new Color(255, 255, 255), new Color(135, 135, 135), new Color(115, 118, 129), new Color(141, 144, 158), new Color(164, 168, 184), new Color(86, 88, 97), new Color(106, 76, 54), new Color(130, 94, 66), new Color(151, 109, 77), new Color(79, 57, 40), new Color(79, 79, 79), new Color(96, 96, 96), new Color(112, 112, 112), new Color(59, 59, 59), new Color(45, 45, 180), new Color(55, 55, 220), new Color(64, 64, 255), new Color(33, 33, 135), new Color(100, 84, 50), new Color(123, 102, 62), new Color(143, 119, 72), new Color(75, 63, 38), new Color(180, 177, 172), new Color(220, 217, 211), new Color(255, 252, 245), new Color(135, 133, 129), new Color(152, 89, 36), new Color(186, 109, 44), new Color(216, 127, 51), new Color(114, 67, 27), new Color(125, 53, 152), new Color(153, 65, 186), new Color(178, 76, 216), new Color(94, 40, 114), new Color(72, 108, 152), new Color(88, 132, 186), new Color(102, 153, 216), new Color(54, 81, 114), new Color(161, 161, 36), new Color(197, 197, 44), new Color(229, 229, 51), new Color(121, 121, 27), new Color(89, 144, 17), new Color(109, 176, 21), new Color(127, 204, 25), new Color(67, 108, 13), new Color(170, 89, 116), new Color(208, 109, 142), new Color(242, 127, 165), new Color(128, 67, 87), new Color(53, 53, 53), new Color(65, 65, 65), new Color(76, 76, 76), new Color(40, 40, 40), new Color(108, 108, 108), new Color(132, 132, 132), new Color(153, 153, 153), new Color(81, 81, 81), new Color(53, 89, 108), new Color(65, 109, 132), new Color(76, 127, 153), new Color(40, 67, 81), new Color(89, 44, 125), new Color(109, 54, 153), new Color(127, 63, 178), new Color(67, 33, 94), new Color(36, 53, 125), new Color(44, 65, 153), new Color(51, 76, 178), new Color(27, 40, 94), new Color(72, 53, 36), new Color(88, 65, 44), new Color(102, 76, 51), new Color(54, 40, 27), new Color(72, 89, 36), new Color(88, 109, 44), new Color(102, 127, 51), new Color(54, 67, 27), new Color(108, 36, 36), new Color(132, 44, 44), new Color(153, 51, 51), new Color(81, 27, 27), new Color(17, 17, 17), new Color(21, 21, 21), new Color(25, 25, 25), new Color(13, 13, 13), new Color(176, 168, 54), new Color(215, 205, 66), new Color(250, 238, 77), new Color(132, 126, 40), new Color(64, 154, 150), new Color(79, 188, 183), new Color(92, 219, 213), new Color(48, 115, 112), new Color(52, 90, 180), new Color(63, 110, 220), new Color(74, 128, 255), new Color(39, 67, 135), new Color(0, 153, 40), new Color(0, 187, 50), new Color(0, 217, 58), new Color(0, 114, 30), new Color(91, 60, 34), new Color(111, 74, 42), new Color(129, 86, 49), new Color(68, 45, 25), new Color(79, 1, 0), new Color(96, 1, 0), new Color(112, 2, 0), new Color(59, 1, 0), new Color(147, 124, 113), new Color(180, 152, 138), new Color(209, 177, 161), new Color(110, 93, 85), new Color(112, 57, 25), new Color(137, 70, 31), new Color(159, 82, 36), new Color(84, 43, 19), new Color(105, 61, 76), new Color(128, 75, 93), new Color(149, 87, 108), new Color(78, 46, 57), new Color(79, 76, 97), new Color(96, 93, 119), new Color(112, 108, 138), new Color(59, 57, 73), new Color(131, 93, 25), new Color(160, 114, 31), new Color(186, 133, 36), new Color(98, 70, 19), new Color(72, 82, 37), new Color(88, 100, 45), new Color(103, 117, 53), new Color(54, 61, 28), new Color(112, 54, 55), new Color(138, 66, 67), new Color(160, 77, 78), new Color(84, 40, 41), new Color(40, 28, 24), new Color(49, 35, 30), new Color(57, 41, 35), new Color(30, 21, 18), new Color(95, 75, 69), new Color(116, 92, 84), new Color(135, 107, 98), new Color(71, 56, 51), new Color(61, 64, 64), new Color(75, 79, 79), new Color(87, 92, 92), new Color(46, 48, 48), new Color(86, 51, 62), new Color(105, 62, 75), new Color(122, 73, 88), new Color(64, 38, 46), new Color(53, 43, 64), new Color(65, 53, 79), new Color(76, 62, 92), new Color(40, 32, 48), new Color(53, 35, 24), new Color(65, 43, 30), new Color(76, 50, 35), new Color(40, 26, 18), new Color(53, 57, 29), new Color(65, 70, 36), new Color(76, 82, 42), new Color(40, 43, 22), new Color(100, 42, 32), new Color(122, 51, 39), new Color(142, 60, 46), new Color(75, 31, 24), new Color(26, 15, 11), new Color(31, 18, 13), new Color(37, 22, 16), new Color(19, 11, 8)};
        for (int x = 0; x < 128; x++) {
            for (int y = 0; y < 128; y++) {
                byte index = img[(y * 128 + x)];
                if (index <= -49 || index >= 0) {
                    image.setRGB(x, y, colors[index >= 0 ? index : index + 256].getRGB());
                }
            }
        }
    }

}
