package pro.eugw.MinecraftBot;

import java.awt.*;
import javax.swing.*;
import javax.swing.GroupLayout;
import javax.swing.text.DefaultCaret;

@SuppressWarnings("FieldCanBeLocal")
public class Console extends JFrame {

    public Console() {
        initComponents();
        DefaultCaret caret = (DefaultCaret) textArea1.getCaret();
        caret.setUpdatePolicy(DefaultCaret.ALWAYS_UPDATE);
        for (BotThread thread : Main.threads) {
            thread.command("/ping", textArea1);
        }
        send.addActionListener(e -> {
            for (BotThread thread : Main.threads) {
                thread.command(textField2.getText(), textArea1);
            }
        });
    }

    private void initComponents() {
        // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
        // Generated using JFormDesigner Evaluation license - Evgeny Brin
        send = new JButton();
        textField2 = new JTextField();
        scrollPane1 = new JScrollPane();
        textArea1 = new JTextArea();

        //======== this ========
        setTitle("Console");
        setIconImage(new ImageIcon("D:\\wg\\Downloads\\Emerald.png").getImage());
        Container contentPane = getContentPane();

        //---- send ----
        send.setText("SEND");

        //======== scrollPane1 ========
        {
            scrollPane1.setAutoscrolls(true);

            //---- textArea1 ----
            textArea1.setCursor(Cursor.getPredefinedCursor(Cursor.TEXT_CURSOR));
            textArea1.setEditable(false);
            scrollPane1.setViewportView(textArea1);
        }

        GroupLayout contentPaneLayout = new GroupLayout(contentPane);
        contentPane.setLayout(contentPaneLayout);
        contentPaneLayout.setHorizontalGroup(
            contentPaneLayout.createParallelGroup()
                .addGroup(GroupLayout.Alignment.TRAILING, contentPaneLayout.createSequentialGroup()
                    .addContainerGap()
                    .addGroup(contentPaneLayout.createParallelGroup(GroupLayout.Alignment.TRAILING)
                        .addComponent(scrollPane1, GroupLayout.DEFAULT_SIZE, 386, Short.MAX_VALUE)
                        .addGroup(contentPaneLayout.createSequentialGroup()
                            .addComponent(textField2, GroupLayout.DEFAULT_SIZE, 315, Short.MAX_VALUE)
                            .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                            .addComponent(send)))
                    .addContainerGap())
        );
        contentPaneLayout.setVerticalGroup(
            contentPaneLayout.createParallelGroup()
                .addGroup(contentPaneLayout.createSequentialGroup()
                    .addContainerGap()
                    .addComponent(scrollPane1, GroupLayout.DEFAULT_SIZE, 221, Short.MAX_VALUE)
                    .addGap(9, 9, 9)
                    .addGroup(contentPaneLayout.createParallelGroup(GroupLayout.Alignment.BASELINE)
                        .addComponent(send, GroupLayout.PREFERRED_SIZE, 26, GroupLayout.PREFERRED_SIZE)
                        .addComponent(textField2, GroupLayout.PREFERRED_SIZE, 26, GroupLayout.PREFERRED_SIZE))
                    .addContainerGap())
        );
        pack();
        setLocationRelativeTo(getOwner());
        // JFormDesigner - End of component initialization  //GEN-END:initComponents
    }

    // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
    // Generated using JFormDesigner Evaluation license - Evgeny Brin
    private JButton send;
    private JTextField textField2;
    private JScrollPane scrollPane1;
    private JTextArea textArea1;
    // JFormDesigner - End of variables declaration  //GEN-END:variables


}
