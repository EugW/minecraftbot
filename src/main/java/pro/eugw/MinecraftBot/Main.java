package pro.eugw.MinecraftBot;

import javax.swing.*;
import java.awt.*;
import java.io.*;
import java.util.ArrayList;
import java.util.Objects;
import java.util.Properties;

import static pro.eugw.MinecraftBot.Misc.log;

@SuppressWarnings("FieldCanBeLocal")
public class Main extends JFrame {

    static ArrayList<BotThread> threads = new ArrayList<>();

    private Main() {
        initComponents();
        start.addActionListener(e -> {
            ArrayList<String> proxy = new ArrayList<>();
            proxy.add("");
            try {
                BufferedReader reader = new BufferedReader(new FileReader(new File(textField2.getText())));
                String rd;
                while ((rd = reader.readLine()) != null) {
                    proxy.add(rd);
                }
            } catch (IOException e1) {
                log().error(e1 + " Cannot read proxy file");
            }
            Integer max = Integer.valueOf(textField3.getText());
            Integer i = 1;
            while (true) {
                try {
                    String ip = textField1.getText();
                    String user = textField5.getText().split(":")[0] + i + ":" +  textField5.getText().split(":")[1];
                    String apiKey = textField4.getText();
                    Bot thread = new Bot(ip, user, apiKey, proxy.get(i));
                    log().info("Bot " + user + " started");
                    threads.add(thread);
                    thread.start();
                } catch (Exception e1) {
                    log().error(e1 + " Cannot create bot");
                }
                if (Objects.equals(max, i))
                    break;
                i++;
            }
        });
        stop.addActionListener(e -> {
            for (BotThread thread : threads) {
                thread.interrupt();
            }
            threads.clear();
        });
        load.addActionListener(e -> {
            File config = new File("config");
            if (config.exists()) {
                try {
                    Properties properties = new Properties();
                    properties.load(new FileInputStream(config));
                    textField1.setText(properties.getProperty("ip"));
                    textField2.setText(properties.getProperty("proxy"));
                    textField3.setText(properties.getProperty("bots"));
                    textField4.setText(properties.getProperty("key"));
                    textField5.setText(properties.getProperty("account"));
                } catch (Exception e1) {
                    log().error("Cannot load config");
                }
            } else {
                try {
                    if (config.createNewFile())
                        log().debug("Config created");
                    Properties properties = new Properties();
                    properties.load(new FileInputStream(config));
                    properties.setProperty("ip", textField1.getText());
                    properties.setProperty("proxy", textField2.getText());
                    properties.setProperty("bots", textField3.getText());
                    properties.setProperty("key", textField4.getText());
                    properties.setProperty("account", textField5.getText());
                    properties.store(new FileOutputStream(config),"settings");
                } catch (IOException e1) {
                    e1.printStackTrace();
                }
            }
        });
        save.addActionListener(e -> {
            File config = new File("config");
            try {
                if (config.createNewFile())
                    log().debug("Config created");
                Properties properties = new Properties();
                properties.load(new FileInputStream(config));
                properties.setProperty("ip", textField1.getText());
                properties.setProperty("proxy", textField2.getText());
                properties.setProperty("bots", textField3.getText());
                properties.setProperty("key", textField4.getText());
                properties.setProperty("account", textField5.getText());
                properties.store(new FileOutputStream(config), "settings");
            } catch (IOException e1) {
                log().error("Cannot save config");
            }
        });
        console.addActionListener(e -> {
            Console console = new Console();
            console.setVisible(true);
        });
        openFile.addActionListener(e -> {
            JFileChooser fileChooser = new JFileChooser();
            fileChooser.setFileSelectionMode(JFileChooser.FILES_ONLY);
            fileChooser.getActionMap().get("viewTypeDetails").actionPerformed(null);
            if (fileChooser.showOpenDialog(rootPane) == 0) {
                textField2.setText(String.valueOf(fileChooser.getSelectedFile()));
            }
        });
    }

    private void initComponents() {
        // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
        // Generated using JFormDesigner Evaluation license - Evgeny Brin
        label1 = new JLabel();
        textField1 = new JTextField();
        label2 = new JLabel();
        textField2 = new JTextField();
        label3 = new JLabel();
        textField3 = new JTextField();
        label4 = new JLabel();
        textField4 = new JTextField();
        start = new JButton();
        stop = new JButton();
        label5 = new JLabel();
        textField5 = new JTextField();
        load = new JButton();
        save = new JButton();
        console = new JButton();
        openFile = new JButton();

        //======== this ========
        setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        setTitle("MinecraftBot");
        setFont(new Font("Consolas", Font.PLAIN, 12));
        setResizable(false);
        setIconImage(new ImageIcon("D:\\wg\\Downloads\\Emerald.png").getImage());
        Container contentPane = getContentPane();

        //---- label1 ----
        label1.setText("IP: ");

        //---- textField1 ----
        textField1.setToolTipText("Server IP");

        //---- label2 ----
        label2.setText("PROXY:");

        //---- textField2 ----
        textField2.setToolTipText("Proxy file");

        //---- label3 ----
        label3.setText("BOTS:");

        //---- textField3 ----
        textField3.setToolTipText("Bots count");

        //---- label4 ----
        label4.setText("RUCAPTCHA API KEY:");

        //---- textField4 ----
        textField4.setToolTipText("RuCaptcha api key");

        //---- start ----
        start.setText("START");
        start.setFont(start.getFont().deriveFont(start.getFont().getSize() + 10f));

        //---- stop ----
        stop.setText("STOP");
        stop.setFont(stop.getFont().deriveFont(stop.getFont().getSize() + 10f));

        //---- label5 ----
        label5.setText("ACCOUNT:");

        //---- textField5 ----
        textField5.setToolTipText("Login:Password");

        //---- load ----
        load.setText("LOAD");
        load.setFont(load.getFont().deriveFont(load.getFont().getSize() + 15f));

        //---- save ----
        save.setText("SAVE");
        save.setFont(save.getFont().deriveFont(save.getFont().getSize() + 15f));

        //---- console ----
        console.setText("CONSOLE");

        //---- openFile ----
        openFile.setText("FILE");

        GroupLayout contentPaneLayout = new GroupLayout(contentPane);
        contentPane.setLayout(contentPaneLayout);
        contentPaneLayout.setHorizontalGroup(
            contentPaneLayout.createParallelGroup()
                .addGroup(contentPaneLayout.createSequentialGroup()
                    .addContainerGap()
                    .addGroup(contentPaneLayout.createParallelGroup()
                        .addGroup(contentPaneLayout.createSequentialGroup()
                            .addGroup(contentPaneLayout.createParallelGroup()
                                .addGroup(contentPaneLayout.createSequentialGroup()
                                    .addComponent(label4)
                                    .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                                    .addComponent(textField4))
                                .addGroup(contentPaneLayout.createSequentialGroup()
                                    .addComponent(label5)
                                    .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                                    .addComponent(textField5))
                                .addComponent(start, GroupLayout.Alignment.TRAILING, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addComponent(stop, GroupLayout.Alignment.TRAILING, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addGroup(contentPaneLayout.createSequentialGroup()
                                    .addGroup(contentPaneLayout.createParallelGroup()
                                        .addGroup(contentPaneLayout.createSequentialGroup()
                                            .addComponent(label1)
                                            .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                                            .addComponent(textField1, GroupLayout.PREFERRED_SIZE, 381, GroupLayout.PREFERRED_SIZE))
                                        .addGroup(contentPaneLayout.createSequentialGroup()
                                            .addComponent(label2)
                                            .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                                            .addComponent(textField2, GroupLayout.PREFERRED_SIZE, 292, GroupLayout.PREFERRED_SIZE)
                                            .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                                            .addComponent(openFile))
                                        .addGroup(contentPaneLayout.createSequentialGroup()
                                            .addComponent(label3)
                                            .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                                            .addComponent(textField3, GroupLayout.PREFERRED_SIZE, 363, GroupLayout.PREFERRED_SIZE)))
                                    .addGap(0, 0, Short.MAX_VALUE)))
                            .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                            .addGroup(contentPaneLayout.createParallelGroup()
                                .addComponent(load, GroupLayout.DEFAULT_SIZE, 146, Short.MAX_VALUE)
                                .addComponent(save, GroupLayout.Alignment.TRAILING, GroupLayout.DEFAULT_SIZE, 146, Short.MAX_VALUE)))
                        .addComponent(console, GroupLayout.Alignment.TRAILING, GroupLayout.DEFAULT_SIZE, 556, Short.MAX_VALUE))
                    .addContainerGap())
        );
        contentPaneLayout.setVerticalGroup(
            contentPaneLayout.createParallelGroup()
                .addGroup(contentPaneLayout.createSequentialGroup()
                    .addContainerGap()
                    .addGroup(contentPaneLayout.createParallelGroup(GroupLayout.Alignment.LEADING, false)
                        .addGroup(contentPaneLayout.createSequentialGroup()
                            .addGroup(contentPaneLayout.createParallelGroup()
                                .addComponent(label1)
                                .addComponent(textField1, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                            .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                            .addGroup(contentPaneLayout.createParallelGroup()
                                .addComponent(label2)
                                .addGroup(GroupLayout.Alignment.TRAILING, contentPaneLayout.createParallelGroup(GroupLayout.Alignment.BASELINE)
                                    .addComponent(openFile, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
                                    .addComponent(textField2, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)))
                            .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                            .addGroup(contentPaneLayout.createParallelGroup(GroupLayout.Alignment.BASELINE)
                                .addComponent(label3)
                                .addComponent(textField3, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                            .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                            .addGroup(contentPaneLayout.createParallelGroup(GroupLayout.Alignment.BASELINE)
                                .addComponent(label4)
                                .addComponent(textField4, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                            .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                            .addGroup(contentPaneLayout.createParallelGroup(GroupLayout.Alignment.BASELINE)
                                .addComponent(label5)
                                .addComponent(textField5, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)))
                        .addComponent(load, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                    .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                    .addGroup(contentPaneLayout.createParallelGroup(GroupLayout.Alignment.LEADING, false)
                        .addGroup(contentPaneLayout.createSequentialGroup()
                            .addComponent(start, GroupLayout.PREFERRED_SIZE, 50, GroupLayout.PREFERRED_SIZE)
                            .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(stop, GroupLayout.PREFERRED_SIZE, 50, GroupLayout.PREFERRED_SIZE))
                        .addComponent(save, GroupLayout.PREFERRED_SIZE, 106, GroupLayout.PREFERRED_SIZE))
                    .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                    .addComponent(console)
                    .addContainerGap(GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        pack();
        setLocationRelativeTo(getOwner());
        // JFormDesigner - End of component initialization  //GEN-END:initComponents
    }

    // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
    // Generated using JFormDesigner Evaluation license - Evgeny Brin
    private JLabel label1;
    private JTextField textField1;
    private JLabel label2;
    private JTextField textField2;
    private JLabel label3;
    private JTextField textField3;
    private JLabel label4;
    private JTextField textField4;
    private JButton start;
    private JButton stop;
    private JLabel label5;
    private JTextField textField5;
    private JButton load;
    private JButton save;
    private JButton console;
    private JButton openFile;
    // JFormDesigner - End of variables declaration  //GEN-END:variables

    public static void main(String[] args) throws ClassNotFoundException, UnsupportedLookAndFeelException, InstantiationException, IllegalAccessException {
        Main GUI = new Main();
        GUI.setVisible(true);
    }

}
