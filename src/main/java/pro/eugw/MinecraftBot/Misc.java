package pro.eugw.MinecraftBot;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

class Misc {

    static Logger log() {
        return LogManager.getLogger();
    }

}
