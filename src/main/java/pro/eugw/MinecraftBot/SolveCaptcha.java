package pro.eugw.MinecraftBot;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.ContentType;
import org.apache.http.entity.mime.MultipartEntityBuilder;
import org.apache.http.entity.mime.content.FileBody;
import org.apache.http.entity.mime.content.StringBody;
import org.apache.http.impl.client.HttpClients;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Objects;
import java.util.regex.Pattern;

class SolveCaptcha {

    private String api_key = "";
    private String response = "";
    private String id = "";
    private String captcha = "";
    private String image = "";


    SolveCaptcha(String image, String api_key) {
        this.image = image;
        this.api_key = api_key;
    }

    String get() {
        sendCaptcha();
        getCaptcha();
        return captcha;
    }

    private void sendCaptcha() {
        HttpClient client = HttpClients.createMinimal();
        HttpPost post = new HttpPost("http://rucaptcha.com/in.php");
        post.setHeader("User-Agent", "MinecraftBot");
        MultipartEntityBuilder builder = MultipartEntityBuilder.create();
        builder.addPart("key", new StringBody(api_key, ContentType.MULTIPART_FORM_DATA));
        builder.addPart("soft_id", new StringBody("1915", ContentType.MULTIPART_FORM_DATA));
        builder.addPart("file", new FileBody(new File(this.image)));
        post.setEntity(builder.build());
        try {
            HttpResponse httpResponse = client.execute(post);
            this.response = new BufferedReader(new InputStreamReader(httpResponse.getEntity().getContent())).readLine();
        } catch (IOException e) {
            e.printStackTrace();
        }
        if (Objects.equals(this.response.split(Pattern.quote("|"))[0], "OK"))
            this.id = this.response.split(Pattern.quote("|"))[1];
    }

    private void getCaptcha() {
        while (true) {
            try {
                Thread.sleep(5000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            HttpClient client = HttpClients.createMinimal();
            HttpGet get = new HttpGet("http://rucaptcha.com/res.php?key=" + api_key + "&action=get&id=" + this.id);
            get.addHeader("User-Agent", "MinecraftBot");
            try {
                HttpResponse response = client.execute(get);
                this.response = new BufferedReader(new InputStreamReader(response.getEntity().getContent())).readLine();
            } catch (IOException e) {
                e.printStackTrace();
            }
            if (Objects.equals(this.response.split(Pattern.quote("|"))[0], "OK")) {
                this.captcha = this.response.split(Pattern.quote("|"))[1];
                break;
            }
        }
    }

}
